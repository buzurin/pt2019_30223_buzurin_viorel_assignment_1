package PT2019.demo.DemoProject;

public class Monom {
	protected int coeficient;
	protected int putere;	
	protected double coef;
	public Monom(int c,int p)
	{
		this.coef=c;
		this.coeficient=c;
		this.putere=p;		
	}
	public Monom(float a,int p)
	{
		this.coef=a;
		this.coeficient= (int) a;
		this.putere=p;		
	}	
	public String afisareMonom(Monom a)
	{
		String s1="";	
		if(a.coeficient==0)
			return "";
		else
		if(a.coeficient==1 && a.putere==1)
		{					
			s1=s1+"+x";				
		}			
		else
		if(a.coeficient!=1 && a.putere>1)
		{
			if(a.coeficient>0)				
				s1=s1+"+"+a.coeficient+"x^"+a.putere;
			else 
				s1=s1+a.coeficient+"x^"+a.putere;
		}
		if(a.coeficient!=1 && a.putere==1)
		{
			if(a.coeficient>0)				
				s1=s1+"+"+a.coeficient+"x";
			else 
				s1=s1+a.coeficient+"x";
		}
		if(a.coeficient==1 && a.putere>1)
		{
			s1=s1+"+"+"x^"+a.putere;
		}
		else 
			if(a.putere==0)
				{
					if(a.coeficient>0)				
						s1=s1+"+"+a.coeficient;
					else 
						s1=s1+a.coeficient;
				}			
		return s1;
		
	}	

}
