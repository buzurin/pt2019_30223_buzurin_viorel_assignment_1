package PT2019.demo.DemoProject;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.*;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.text.DecimalFormat;

public class Test {
	public ArrayList<Monom>  transforma(String x1,ArrayList<Monom> l)
	{					
		String x2;
		int c=0,p=0;			
		while(x1!=null)
		{
			if(x1.indexOf("x^")>-1) 
			{
				if(x1.indexOf("+")==0)
				{
					x1=x1.substring(1);	
					x2=x1.substring(0,x1.indexOf("x^"));
					c=Integer.parseInt(x2);
				}
				else if(x1.indexOf("-")==0)
				{
					x1=x1.substring(1);
					x2=x1.substring(0,x1.indexOf("x^"));
					c=-(Integer.parseInt(x2));
				}
				else
				{
					x2=x1.substring(0,x1.indexOf("x^"));
					c=(Integer.parseInt(x2));
				}
				if(x1.indexOf("+")<x1.indexOf("-") || x1.indexOf("-")==-1)
				{					
					x2=x1.substring(x1.indexOf("x^")+2,x1.indexOf("+"));
				}
				else
					x2=x1.substring(x1.indexOf("x^")+2,x1.indexOf("-"));				
				p=Integer.parseInt(x2);			
				x1=x1.substring(x1.indexOf("^")+x2.length()+1);				
			}
			else
			{
				if(x1.indexOf("x")>-1)
				{	
					if(x1.indexOf("+")==0)
					{
						x2=x1.substring(1,x1.indexOf("x"));
						c=Integer.parseInt(x2);	
					}
					else
					if(x1.indexOf("-")==0)
					{
						x2=x1.substring(1,x1.indexOf("x"));
						c=-Integer.parseInt(x2);	
					}
					else
					{
						x2=x1.substring(0,x1.indexOf("x"));
						c=Integer.parseInt(x2);
					}
					x1=x1.substring(x1.indexOf("x")+1);	
					p=1;
				}
				else
				{
					if(x1.indexOf("+")>-1)
					{
						c=Integer.parseInt(x1.substring(1));						
						p=0;
						x1=null;						
					}
					else
					if(x1.indexOf("-")>-1)
					{
						c=Integer.parseInt(x1.substring(1));
						c=-c;	
						p=0;
						x1=null;
					}
					else 
					{
						x1=null;
						c=p=0;
					}
				}
			}			
			l.add(new Monom(c,p));		
		}	
		return l;
	}	
	public static void main(String[] args) {
		// TODO Auto-generated method stub			
		JFrame frame = new JFrame ("Polinom");
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setSize(600, 400);
		 Color cool = new Color(235, 35, 123);
		// frame.setBackground(cool);
		 JPanel panel1 = new JPanel();
		 JPanel panel2 = new JPanel();
		// JPanel panel3 =new JPanel();
		 final JTextField tf = new JTextField (null);
		 final JTextField tf1 = new JTextField(null);	
		 final JTextField tf2 = new JTextField("");
		 tf2.setEditable(false);
		 JLabel lb=new JLabel("Polinom1:");
		 JLabel lb1=new JLabel("Polinom2:");
		 JLabel lb3=new JLabel("Rezultat:");
		 panel1.add(lb);
		 panel1.add(tf);
		 panel1.add(lb1);
		 panel1.add(tf1);
		 panel1.add(lb3);
		 panel1.add(tf2);
		 panel1.setLayout(new GridLayout(0,2));
		 panel2.setBackground(cool);
		 JButton b1 = new JButton("Aduna");
		 JButton b2 = new JButton("Scade");
		 JButton b3 = new JButton("Imulteste");
		 JButton b4 = new JButton("Imparte");
		 JButton b5 = new JButton("Deriveaza");
		 JButton b6 = new JButton("Integreaza");
		 panel2.setLayout(new GridLayout(3,3));
		 panel2.add(b1);
		 panel2.add(b2);
		 panel2.add(b3);
		 panel2.add(b4);
		 panel2.add(b5);
		 panel2.add(b6);

		 JPanel p = new JPanel();
		 p.add(panel1);
		 p.add(panel2);
		 p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		 frame.setContentPane(p);
		 frame.setVisible(true); 
		
		final Test t=new Test();			
		b1.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;
				String x2;
				ArrayList<Monom> l1=new ArrayList<Monom>();
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);	
				Polinom p2=new Polinom(l1);	
				x1=tf.getText();
				x2=tf1.getText();
				try{					
					l=t.transforma(x1, l);				
					l1=t.transforma(x2, l1);					
					p1=p1.adunaPolinom(p1, p2);
					p1=p1.sortP(p1);
					tf2.setText(p1.toString(p1,1));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}	
			}
		});		
		b2.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;
				String x2;
				ArrayList<Monom> l1=new ArrayList<Monom>();
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);	
				Polinom p2=new Polinom(l1);	
				x1=tf.getText();
				x2=tf1.getText();
				try{
						l=t.transforma(x1, l);					
						l1=t.transforma(x2, l1);						
						p1=p1.scadePolinom(p1, p2);
						p1=p1.sortP(p1);
						tf2.setText(p1.toString(p1,1));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}	
			}
		});	
		b3.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;
				String x2;
				ArrayList<Monom> l1=new ArrayList<Monom>();
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);	
				Polinom p2=new Polinom(l1);	
				x1=tf.getText();
				x2=tf1.getText();
				try{
					  l=t.transforma(x1, l);				
					  l1=t.transforma(x2, l1);					 
					  p1=p1.imultestePolinom(p1, p2);
					  p1=p1.sortP(p1);
					  tf2.setText(p1.toString(p1,1));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}	
			}
		});	
		b4.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;
				String x2;
				ArrayList<Monom> l1=new ArrayList<Monom>();
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);	
				Polinom p2=new Polinom(l1);	
				x1=tf.getText();
				x2=tf1.getText();
				try{
					 l=t.transforma(x1, l);				
					 l1=t.transforma(x2, l1);					 
					 p1=p2.impartirePolinom(p1, p2);
					 p1=p1.sortP(p1);
					 p2=p2.sortP(p2);
					 tf2.setText(p1.toString(p1,0)+" Rest:"+p2.toString(p2,0));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}	
			}
		});	
		b5.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;				
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);					
				x1=tf.getText();				
				try{
					l=t.transforma(x1, l);						
					p1=p1.derivarePolinom(p1);
					p1=p1.sortP(p1);					
					if(p1.toString(p1,0)=="")
						tf2.setText("0");
					else 
						tf2.setText(p1.toString(p1,1));	
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}				
			}
		});	
		b6.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String x1;				
				ArrayList<Monom> l=new ArrayList<Monom>();
				Polinom p1=new Polinom(l);					
				x1=tf.getText();				
				try
				{
					l=t.transforma(x1, l);					
					p1=p1.sortP(p1);
					p1=p1.integrarePolinom(p1);
					p1=p1.sortP(p1);
					tf2.setText(p1.toString(p1,0));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,"Polinomul nu este introdus corect");
				}	
			}
		});		
	}

}
