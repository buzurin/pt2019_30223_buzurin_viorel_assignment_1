
package PT2019.demo.DemoProject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom extends Monom {
	private ArrayList<Monom> polinom=new ArrayList<Monom>();
	
	public Polinom(ArrayList<Monom> l) 
	{
		super(0, 0);
		this.polinom=l;				
	}
	public Polinom sortP(Polinom a)
	{
		Collections.sort(a.polinom, new Sortbyroll()); 
		return a;
	}
	public Polinom adunaPolinom(Polinom a,Polinom b)
	{
		ArrayList<Monom> l=new ArrayList<Monom>();
		Polinom p=new Polinom(l);		
		int ok,i=0,p1;
		double c1;
		for(Monom m1:b.polinom)
			p.polinom.add(m1);		
		for(Monom m1:a.polinom)
		{
			ok=0;	
			i=0;
			for(Monom m2:b.polinom)
			{					
				if(m1.putere==m2.putere)
				{					
					p1=m2.putere;
					c1=m1.coef+m2.coef;
				    p.polinom.remove(i);
				    p.polinom.add(i,new Monom((float)c1,p1));
					ok=1;
				}
				i++;
			}			
			if(ok==0)
				p.polinom.add(m1);							
		}		
		return p;
	}
	public Polinom scadePolinom(Polinom a,Polinom b)
	{
		ArrayList<Monom> l=new ArrayList<Monom>();
		Polinom p=new Polinom(l);		
		int ok,i=0,p1,c1;
		for(Monom m1:b.polinom)
			p.polinom.add(m1);		
		for(Monom m1:a.polinom)
		{
			ok=0;	
			i=0;
			for(Monom m2:b.polinom)
			{					
				if(m1.putere==m2.putere)
				{					
					p1=m2.putere;
					c1=(int) (m1.coeficient-m2.coeficient);
				    p.polinom.remove(i);
				    p.polinom.add(i,new Monom(c1,p1));
					ok=1;
				}
				i++;
			}			
			if(ok==0)
				p.polinom.add(m1);							
		}
		return p;
	}
	public Polinom imultestePolinom(Polinom a,Polinom b)
	{
		ArrayList <Monom> l=new ArrayList<Monom>();
		ArrayList<Monom>l1=new ArrayList<Monom>();
		Polinom p2=new Polinom(l1);
		Polinom p=new Polinom(l);
		double c1;
		int p1;
		for(Monom m1:a.polinom)
		{
			for(Monom m2:b.polinom)
			{
				c1=m1.coef*m2.coef;
				p1=m1.putere+m2.putere;
				p.polinom.add(new Monom((float)c1,p1));
			}
			p2=p2.adunaPolinom(p2, p);
			p.polinom.removeAll(p.polinom);
		}
		return p2;
	}
	public Polinom derivarePolinom(Polinom a)
	{
		ArrayList<Monom> l=new ArrayList<Monom>();
		Polinom p=new Polinom(l);
		int c1,p1;
		for(Monom m:a.polinom)
		{
			c1=(int) (m.coeficient*m.putere);
			p1=m.putere-1;
			p.polinom.add(new Monom(c1,p1));
		}		
		return p;		
	}
	public Polinom integrarePolinom(Polinom a)
	{
		ArrayList<Monom>l=new ArrayList<Monom>();
		Polinom p=new Polinom(l);
		float c1;
		int p1;
		for(Monom m1:a.polinom)
		{
			c1=(float)(m1.coef*(1.0/(m1.putere+1)));
			p1=m1.putere+1;
			p.polinom.add(new Monom(c1,p1));
		}
		return p;				
	}
	public Monom cautaMax(Polinom p)
	{		
		
		Monom m=new Monom((float)-99,-99);
		for(Monom m1:p.polinom)
		{
			if(m.putere<m1.putere && m1.coef!=0)
			{
				m.putere=m1.putere;				
				m.coef= m1.coef;
				m.coeficient=(int)m1.coef;
			}
		}
		return m;
	}
	public Polinom impartirePolinom(Polinom a,Polinom b)
	{
		ArrayList<Monom> l=new ArrayList<Monom>();			
		ArrayList<Monom> l3=new ArrayList<Monom>();	
		Polinom p=new Polinom(l3);
		Monom m=new Monom(0,0);
		Monom m2=new Monom(0,0);		
		double c;
		int pu;		
		m=a.cautaMax(a);
		m2=b.cautaMax(b);		
		while(m.putere >= m2.putere)
		{							
			pu=m.putere-m2.putere;
			c=-(m.coef/m2.coef);			
			l.add(new Monom((float)-c,pu));
			p.polinom.add(new Monom((float)c,pu));				
			p=p.imultestePolinom(p,b);					
			a=a.adunaPolinom(a,p);				
			p.polinom.clear();
			m=a.cautaMax(a);			
		}
		Polinom cat=new Polinom(l);	
		this.polinom=a.polinom;
		return cat;
	}
	static double intMargin = 1e-14;
	public static String myFormat(double d) {
	    DecimalFormat format;
	    // is value an integer?
	    if (Math.abs(d - Math.round(d)) < intMargin) { // close enough
	        format = new DecimalFormat("#,##0.#");
	    } else {
	        format = new DecimalFormat("#,##0.0");
	    }
	    return format.format(d);
	}
	public String toString(Polinom p,int t)
	{
		//System.out.print("da");
		if(t==0)
		{			
			String s1="";
			for(Monom s:p.polinom)
			{
				if(s.coef>0)					
						s1=s1+("+"+myFormat(s.coef)+"x^"+s.putere);					
				else
					if(s.coef<0)
						s1=s1+(myFormat(s.coef)+"x^"+s.putere);
			}
			if(s1.indexOf("+")==0)
				s1=s1.substring(1, s1.length());
			return s1;
		}
		else 
		{	
			String s1="";
			for(Monom m1:p.polinom)
			{
				s1=s1+m1.afisareMonom(m1);
			}
			if(s1.indexOf("+")==0)
				s1=s1.substring(1, s1.length());
			return s1;
	    }
	}	
}
