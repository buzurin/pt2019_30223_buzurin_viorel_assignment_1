package PT2019.demo.DemoProject;
import java.util.ArrayList;

import junit.framework.TestCase;

public class TestFixture extends TestCase {
	private  Polinom p1;
	private  Polinom p2;

	//Setare polinoame
	public void setUp() {
		ArrayList<Monom> l1=new  ArrayList<Monom>();
		ArrayList<Monom> l2=new  ArrayList<Monom>();
		l1.add(new Monom(1,1));
		l1.add(new Monom(1,3));
		l1.add(new Monom(1,0));
		l2.add(new Monom(1,1));
		l2.add(new Monom(1,0));		
		p1 = new Polinom(l1);
		p2 = new Polinom(l2);

		System.out.println("setUp: p1: " + p1.toString(p1,1) + ", p2: " + p2.toString(p2,1));
	}

	// operatii cu cele doua polinoame
	public void testAdd()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom(2,1));
		l3.add(new Monom(2,0));
		l3.add(new Monom(1,3));
		Polinom p3=new Polinom(l3);		
		p1=p1.adunaPolinom(p1, p2);		
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
	}
	public void testSub()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom(1,3));		
		Polinom p3=new Polinom(l3);		
		p1=p1.scadePolinom(p1, p2);			
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
	}
	public void testMult()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom(2,1));
		l3.add(new Monom(1,0));
		l3.add(new Monom(1,4));
		l3.add(new Monom(1,3));
		l3.add(new Monom(1,2));
		Polinom p3=new Polinom(l3);		
		p1=p1.imultestePolinom(p1, p2);		
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
	}

	public void testDiv()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom(1,2));
		l3.add(new Monom(-1,1));
		l3.add(new Monom(2,0));
		Polinom p3=new Polinom(l3);	
		ArrayList<Monom> l4=new  ArrayList<Monom>();
		l4.add(new Monom(-1,0));		
		Polinom p4=new Polinom(l4);	
		p1=p2.impartirePolinom(p1, p2);			
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
		assertEquals(p4.toString(p4,1),p2.toString(p2,1));
	}
	public void testDerivare()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom(3,2));
		l3.add(new Monom(1,0));		
		Polinom p3=new Polinom(l3);			
		p1=p1.derivarePolinom(p1);
		p1=p1.sortP(p1);		
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
	}
	public void testIntegrare()
	{
		ArrayList<Monom> l3=new  ArrayList<Monom>();
		l3.add(new Monom((float)0.2,4));
		l3.add(new Monom((float)0.5,2));
		l3.add(new Monom((float)1,1));	
		Polinom p3=new Polinom(l3);			
		p1=p1.integrarePolinom(p1);		
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));
	}
	public void testCautaMax()
	{
		Monom m=new Monom(0,0);		
		m=p1.cautaMax(p1);		
		assertTrue(m.coeficient==1);
		assertTrue(m.putere==3);
	}
	public void testtoString()
	{
		String s1="x^3+x+1";
		p1=p1.sortP(p1);		
		assertEquals(p1.toString(p1,1),s1);
		s1="x+1";
		assertEquals(p2.toString(p2,1),s1);
	}
	public void testTransforma()
	{
		String s1="1x^3+1x+1";
		ArrayList<Monom>l=new ArrayList<Monom>();
		Test t=new Test();
		l=t.transforma(s1, l);
		Polinom p3=new Polinom(l);
		p1=p1.sortP(p1);		
		assertEquals(p1.toString(p1,1),p3.toString(p3,1));			
	}	
}

